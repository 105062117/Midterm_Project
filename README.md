# Software Studio 2018 Spring Midterm Project

## Topic
* Chat room

* Key functions
    1. chat
    2. load message history
    3. chat with new user
* Other functions
    1. upload picture
    2. usage of emoji
    3. history message load
    4. email verify

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|20%|Y|
|GitLab Page|5%|Y|
|Database|15%|Y|
|RWD|15%|Y|
|Topic Key Function|15%|Y|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|Y|
|Chrome Notification|5%|Y|
|Use CSS Animation|2.5%|Y|
|Security Report|5%|Y|
|Other functions|1~10%|Y|

## Website Detail Description   
```website address:```https://software-midterm-2018.firebaseapp.com
**1.(sign up/sign in)**   
在進入聊天室頁面後，如果使用者已經註冊帳號過則能直接登入，若是新使用者，可以使用sign up註冊新帳號，在這裡的功能都藏在右上角的menu中，點按後即可展開選者想使用的功能，而輸入email密碼的輸入欄則在menu旁邊，在這裡的登入方式有兩種，google登入或使用email創建新帳號。

**2.(load history message)**   
在確認使用者已經登入且已經經過驗證過後，便可以看到過往的訊息記錄，實作的方法是先取得儲存訊息的位置後，再一筆一筆將訊息輸出在螢幕上，並且監聽是否有新的訊息傳入資料庫，需要進行處理後輸出。訊息儲存的方式是使用者名稱，使用者照片，以及所傳輸的訊息三項資訊。
**3.(hrome Notification)**   
當有人傳送一份訊息出來，且你剛好在線上，則會跳出一個通知，提醒你有人傳送新訊息，當瀏覽器一打開的時候能夠選擇是否允許通知，也可以隨時將通知允許後即可擁有此功能。   
**4.(search friend)**   
在登入的情況下，在右上角輸入想要尋找的使用者名稱後，能夠判斷這名使用這是否存在為你的好友欄中。
**5.(post image)**   
除了一般的訊息傳輸之外，還能夠上傳圖片給好友，儲存圖片的方式跟文字其實類似，只是在前面會限制上傳的檔案必須是圖片，其餘檔案皆會被擋下來。
**6.(post emoji)**   
在傳送訊息旁邊有一個表情符號的按鈕欄，裡面共有十種不同的表情符號，表情符號其實已經在utf-8 的編碼當中被包括了，因此只需在前面加上特殊符號後，便可以像文字一樣按下按鈕後即顯示在輸入欄上。 
**7.(CSS animation)**
在上傳圖片所需時間較長時，會出現一個旋轉的小動畫表示檔案上傳中。標題上則令有一個跳動的動畫，標題按照時間順序上下移動，綜合起來便可達到簡易跳動的效果。     
**8(database usuage)**
使用者名單有而外創立一份在database中存放，裡面的資訊有email,photo,displayname,uid等四項資訊。而一般訊息則會儲存在messages中，儲存資料如前述共有三項資訊，分別是傳送人，傳送者照片跟傳送訊息。    
**9(Email verify)**    
信箱驗證方式為：當新的帳號signup過後，便會收到一封驗證信，點擊信中的連結便可完成驗證，此時再回到聊天室signin，即可使用此聊天室的功能。
**10(background music)**
在聊天的同時，有設置背景音樂播放，並且能使用瀏覽器決定是否該允許播放此音樂。

**11(css display)**
在這裡背景圖的rwd是使用cover的特性去達成效果，因此背景會隨著視窗大小做調整，而網頁上各個元素也使用mdl的設定達到比較特別的按鍵及排版動畫效果，大幅度縮減設計在那些功能上的css code。

## Security Report (Optional)
1. 使用者需要登入且驗證後，才能看到聊天室歷史紀錄以及使用其餘功能。   
2. 使用者需要經過信箱認證才可以發入使用，假使未登入或未驗證是完全無法進行聊天功能的。
3. 若有有心人士，想在訊息欄使用html的tag更改網頁的頁面與功能，如增加一個button之類，由於儲存文字的方式不是直接輸出至innerhtml，因此這類型破壞不會成功。
