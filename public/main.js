// Initializes Chat.
function Chat() {
  this.checkSetup();

  // Shortcuts to DOM Elements.
  this.messageList = document.getElementById('messages');
  this.messageForm = document.getElementById('message-form');
  this.messageInput = document.getElementById('message');
  this.submitButton = document.getElementById('submit');
  this.submitImageButton = document.getElementById('submitImage');
  this.imageForm = document.getElementById('image-form');
  this.mediaCapture = document.getElementById('mediaCapture');
  this.userPic = document.getElementById('user-pic');
  this.userName = document.getElementById('user-name');
  this.signInButton = document.getElementById('sign-in');
  this.signInGoogle = document.getElementById('sign-in-google');
  this.signOutButton = document.getElementById('sign-out');
  this.inputfriend = document.getElementById('friendname');
  this.searchBtn = document.getElementById('search');
  this.signUpButton = document.getElementById('sign-up');
  this.inputemail = document.getElementById('inputEmail');
  this.inputpwd = document.getElementById('inputPassword');
  this.signInSnackbar = document.getElementById('must-signin-snackbar');
  
  //emoji
  this.emojiBtn = document.getElementById('emojisubmit');
  this.emojiBtn1 = document.getElementById('grin');
  this.emojiBtn2 = document.getElementById('sweat');
  this.emojiBtn3 = document.getElementById('kiss');
  this.emojiBtn4 = document.getElementById('think');
  this.emojiBtn5 = document.getElementById('hug');
  this.emojiBtn6 = document.getElementById('sleepy');
  this.emojiBtn7 = document.getElementById('confuse');
  this.emojiBtn8 = document.getElementById('cry');
  this.emojiBtn9 = document.getElementById('heart');
  this.emojiBtn10 = document.getElementById('drool');

  this.emojiBtn1.addEventListener('click', this.emoji1.bind(this));
  this.emojiBtn2.addEventListener('click', this.emoji2.bind(this));
  this.emojiBtn3.addEventListener('click', this.emoji3.bind(this));
  this.emojiBtn4.addEventListener('click', this.emoji4.bind(this));
  this.emojiBtn5.addEventListener('click', this.emoji5.bind(this));
  this.emojiBtn6.addEventListener('click', this.emoji6.bind(this));
  this.emojiBtn7.addEventListener('click', this.emoji7.bind(this));
  this.emojiBtn8.addEventListener('click', this.emoji8.bind(this));
  this.emojiBtn9.addEventListener('click', this.emoji9.bind(this));
  this.emojiBtn10.addEventListener('click', this.emoji10.bind(this));


  // Saves message on form submit.
  this.messageForm.addEventListener('submit', this.saveMessage.bind(this));
  this.signOutButton.addEventListener('click', this.signOut.bind(this));
  this.signInGoogle.addEventListener('click', this.googlesignIn.bind(this));
  this.signInButton.addEventListener('click', this.signIn.bind(this));
  this.signUpButton.addEventListener('click', this.signUp.bind(this));
  this.searchBtn.addEventListener('click', this.search.bind(this));
  

  // Toggle for the button.
  var buttonTogglingHandler = this.toggleButton.bind(this);
  this.messageInput.addEventListener('keyup', buttonTogglingHandler);
  this.messageInput.addEventListener('change', buttonTogglingHandler);
  this.emojiBtn.addEventListener('mouseout' ,buttonTogglingHandler);

  // Events for image upload.
  this.submitImageButton.addEventListener('click', function(e) {
    e.preventDefault();
    this.mediaCapture.click();
  }.bind(this));
  this.mediaCapture.addEventListener('change', this.saveImageMessage.bind(this));

  this.initFirebase();
}

// Sets up shortcuts to Firebase features and initiate firebase auth.
Chat.prototype.initFirebase = function() {
  // Shortcuts to Firebase SDK features.
  this.auth = firebase.auth();
  this.database = firebase.database();
  this.storage = firebase.storage();
  // Initiates Firebase auth and listen to auth state changes.
  this.auth.onAuthStateChanged(this.onAuthStateChanged.bind(this));
};

// Loads chat messages history and listens for upcoming ones.
Chat.prototype.loadMessages = function() {
  // Reference to the /messages/ database path.
  this.messagesRef = this.database.ref('messages');
  // Make sure we remove all previous listeners.
  this.messagesRef.off();

  // Loads the last 12 messages and listen for new ones.
  var setMessage = function(data) {
    var val = data.val();
    this.displayMessage(data.key, val.name, val.text, val.photoUrl, val.imageUrl);
  }.bind(this);
  this.messagesRef.on('child_added', setMessage);
  this.messagesRef.on('child_changed', setMessage);

   if (this.checkSignedInWithMessage())
     this.notify(this.auth.currentUser);
};

// Saves a new message on the Firebase DB.
Chat.prototype.saveMessage = function(e) {
  e.preventDefault();

  // Check that the user entered a message and is signed in.
  if (this.messageInput.value && this.checkSignedInWithMessage()) {
    var currentUser = this.auth.currentUser;
    // Add a new message entry to the Firebase Database.
    this.messagesRef.push({
      name: currentUser.displayName || currentUser.email,
      text: this.messageInput.value,
      photoUrl: currentUser.photoURL || '/images/profile_placeholder.png'
    }).then(function() {
      // Clear message text field and SEND button state.
      Chat.resetMaterialTextfield(this.messageInput);
      this.toggleButton();
    }.bind(this)).catch(function(error) {
      console.error('Error writing new message to Firebase Database', error);
    });
  }
};

//search
Chat.prototype.search = function(){
  //location.reload(true); 

  var userRef = firebase.database().ref('users');
  var msgRef = firebase.database().ref('private');
  var frienduid = this.auth.currentUser.uid;
  var friendname = this.inputfriend.value;
  var current = this.auth.currentUser;
  var exist = false;
  var old_room = true;
  
  userRef.once('value').then(function (snapshot) {
    snapshot.forEach(function (childSnapshot) {
      var name = childSnapshot.val();
      
      if(friendname== name.showname)
      {
        exist = true;
        frienduid = name.uid;
        return true;
      }
    })
  }).then(function(){
      if(exist)
      {
        msgRef.once('value').then(function (snapshot) {
          snapshot.forEach(function (childSnapshot) {
            var userid = childSnapshot.val();
            
            if((userid.uid1 == current.uid && userid.uid2 == frienduid)|| (userid.uid2 == current.uid && userid.uid1 == frienduid))
            {
              //Chat.prototype.loadMessages('private/'+childSnapshot.key);
              old_room = false;
            }
          });
        }).then(function (snapshot){
          if(old_room)
          {
            var postRef = firebase.database().ref('private');

            var data={
              uid1: current.uid,
              uid2: frienduid
            }
            postRef.push(data);
            old_room = true;

            // msgRef.once('value').then(function (snapshot) {
            //   snapshot.forEach(function (childSnapshot) {
            //     var userid = childSnapshot.val(); 
  
            //     if((userid.uid1 == current.uid && userid.uid2 == frienduid)|| (userid.uid2 == current.uid && userid.uid1 == frienduid))
            //     {
            //       Chat.prototype.loadMessages('private/'+childSnapshot.key);
            //     }
            //   });
            // })
          }
          else
            console.log('old room');
        });
      }
      else
        window.alert("User doesn't exist");
   })
}
//Notification

Chat.prototype.notify = function(currentUser){
  if (Notification.permission !== "granted")
    Notification.requestPermission();
  else {
    var postsRef = firebase.database().ref('messages');

    var first_count = 0;
    var second_count = 0;

    postsRef.once('value').then(function (snapshot) {
      snapshot.forEach(function (childSnapshot) {
        first_count += 1;
     });

      postsRef.on('child_added', function (data) {
        second_count += 1;
        if (second_count > first_count) {
          var notification = new Notification('Notification title', {
            icon: '/images/profile_placeholder.png',
            body: "You've new Messege!",
          });
        }
      });
    }).catch(e => console.log(e.message));
  }
};

// Sets the URL of the given img element with the URL of the image stored in Cloud Storage.
Chat.prototype.setImageUrl = function(imageUri, imgElement) {
  // If the image is a Cloud Storage URI we fetch the URL.
  if (imageUri.startsWith('gs://')) {
    imgElement.src = Chat.LOADING_IMAGE_URL; // Display a loading image first.
    this.storage.refFromURL(imageUri).getMetadata().then(function(metadata) {
      imgElement.src = metadata.downloadURLs[0];
    });
  } else {
    imgElement.src = imageUri;
  }
};

// Saves a new message containing an image URI in Firebase.
// This first saves the image in Firebase storage.
Chat.prototype.saveImageMessage = function(event) {
  event.preventDefault();
  var file = event.target.files[0];

  // Clear the selection in the file picker input.
  this.imageForm.reset();

  // Check if the file is an image.
  if (!file.type.match('image.*')) {
    var data = {
      message: 'You can only share images',
      timeout: 2000
    };
    this.signInSnackbar.MaterialSnackbar.showSnackbar(data);
    return;
  }

  // Check if the user is signed-in
  if (this.checkSignedInWithMessage()) {

    // We add a message with a loading icon that will get updated with the shared image.
    var currentUser = this.auth.currentUser;
    this.messagesRef.push({
      name: currentUser.displayName || currentUser.email,
      imageUrl: Chat.LOADING_IMAGE_URL,
      photoUrl: currentUser.photoURL || '/images/profile_placeholder.png'
    }).then(function(data) {

      // Upload the image to Cloud Storage.
      var filePath = currentUser.uid + '/' + data.key + '/' + file.name;
      return this.storage.ref(filePath).put(file).then(function(snapshot) {

        // Get the file's Storage URI and update the chat message placeholder.
        var fullPath = snapshot.metadata.fullPath;
        return data.update({imageUrl: this.storage.ref(fullPath).toString()});
      }.bind(this));
    }.bind(this)).catch(function(error) {
      console.error('There was an error uploading a file to Cloud Storage:', error);
    });
  }
};
//Build List
Chat.prototype.build = function(user) {
  var uniqueRef = firebase.database().ref('users/'+user.uid);
  var listRef = firebase.database().ref('users');

  var isNameExist = false;
  var showname = user.displayName || user.email;
  var data = {
    uid: user.uid,
    email: user.email,
    showname: user.displayName || user.email,
    photoURL: '/images/profile_placeholder.png'      
  }

  listRef.once('value').then(function (snapshot){
    snapshot.forEach(function (childshot) { // childshot => user
      var data = childshot.val();   
      if (showname == data.showname) {
          isNameExist = true;
          return true;
      }   
    });

    uniqueRef.once('value').then(function (snapshot) {
      if(isNameExist)
        Window.alert('Your name has already been used');
      else{
        var userData = snapshot.val();
  
        if(!snapshot.exists())
          uniqueRef.set(data);
      }
    });

  });
}

//Sign-in
Chat.prototype.signIn = function() {
  var email = this.inputemail.value;
  var password = this.inputpwd.value;
  
  firebase.auth().signInWithEmailAndPassword(email, password)
    .catch(e => window.alert(e.message),
    email = "",
    password = "")
    .then(function(user){
      console.log(user);
      this.build(this.auth.currentUser);
 }.bind(this));
};
//Sign Up
Chat.prototype.signUp = function() {
  var email = this.inputemail.value;
  var password = this.inputpwd.value;
  
  this.auth.createUserWithEmailAndPassword(email, password).then(function (user) {
      window.alert("Sign up success!");
      email = "";
      password = "";
  }).catch(e => console.log(e.message));
};

// Signs-in Google
Chat.prototype.googlesignIn = function() {
  // Sign in Firebase using popup auth and Google as the identity provider.
  var provider = new firebase.auth.GoogleAuthProvider();
  this.auth.signInWithPopup(provider).then(function(user){
    console.log(user);
    this.build(this.auth.currentUser);
  }.bind(this));

};

// Signs-out
Chat.prototype.signOut = function() {
  // Sign out of Firebase.
  this.auth.signOut();
  location.reload(true); 
};

// Triggers when the auth state change for instance when the user signs-in or signs-out.
Chat.prototype.onAuthStateChanged = function(user) {
  if (user) { // User is signed in!
    
    if(!this.auth.currentUser.emailVerified){
      this.auth.currentUser.sendEmailVerification().then(function() {
        window.alert("Verification email has been sent");
        console.log("驗證信寄出");
      }, function(error) {
        console.error("驗證信錯誤");
      });
      this.auth.signOut();
    }

    // Get profile pic and user's name from the Firebase user object.
    var profilePicUrl = user.photoURL;
    var userName = user.displayName || user.email;

    // Set the user's profile pic and name.
    this.userPic.style.backgroundImage = 'url(' + (profilePicUrl || '/images/profile_placeholder.png') + ')';
    this.userName.textContent = userName;

    // Show user's profile and sign-out button.
    this.userName.removeAttribute('hidden');
    this.userPic.removeAttribute('hidden');
    this.signOutButton.removeAttribute('hidden');
    
    this.inputfriend.removeAttribute('hidden');
    this.searchBtn.removeAttribute('hidden');

    // Hide sign-in button email pwd and sign-up
    this.signInButton.setAttribute('hidden', 'true');
    this.signInGoogle.setAttribute('hidden', 'true');
    this.inputemail.setAttribute('hidden', 'true');
    this.inputpwd.setAttribute('hidden', 'true');
    this.signUpButton.setAttribute('hidden', 'true');

    //Clear input while Sign-in
    this.inputemail.value="";
    this.inputpwd.value="";

    // We load currently existing chant messages.
    this.loadMessages();

    // We save the Firebase Messaging Device token and enable notifications.

  } else { // User is signed out!
    // Hide user's profile and sign-out button.
    this.userName.setAttribute('hidden', 'true');
    this.userPic.setAttribute('hidden', 'true');
    this.signOutButton.setAttribute('hidden', 'true');
    
    this.inputfriend.setAttribute('hidden', 'true');
    this.searchBtn.setAttribute('hidden', 'true');

    // Show sign-in button email pwd and sign-up
    this.signInButton.removeAttribute('hidden');
    this.signInGoogle.removeAttribute('hidden');
    this.inputemail.removeAttribute('hidden');
    this.inputpwd.removeAttribute('hidden');
    this.signUpButton.removeAttribute('hidden');
  }
};

// Returns true if user is signed-in. Otherwise false and displays a message.
Chat.prototype.checkSignedInWithMessage = function() {
  // Return true if the user is signed in Firebase
  if (this.auth.currentUser) {
    return true;
  }

  // Display a message to the user using a Toast.
  var data = {
    message: 'You must sign-in first',
    timeout: 2000
  };
  this.signInSnackbar.MaterialSnackbar.showSnackbar(data);
  return false;
};

// Resets the given MaterialTextField.
Chat.resetMaterialTextfield = function(element) {
  element.value = '';
  element.parentNode.MaterialTextfield.boundUpdateClassesHandler();
};

// Template for messages.
Chat.MESSAGE_TEMPLATE =
    '<div class="message-container">' +
      '<div class="spacing"><div class="pic"></div></div>' +
      '<div class="message"></div>' +
      '<div class="name"></div>' +
    '</div>';

// A loading image URL.
Chat.LOADING_IMAGE_URL = 'https://www.google.com/images/spin-32.gif';

// Displays a Message in the UI.
Chat.prototype.displayMessage = function(key, name, text, picUrl, imageUri) {

  var div = document.getElementById(key);
  // If an element for that message does not exists yet we create it.
  if (!div) {
    var container = document.createElement('div');
    container.innerHTML = Chat.MESSAGE_TEMPLATE;
    div = container.firstChild;
    div.setAttribute('id', key);
    this.messageList.appendChild(div);
  }
  if (picUrl) {
    div.querySelector('.pic').style.backgroundImage = 'url(' + picUrl + ')';
  }
  div.querySelector('.name').textContent = name;
  var messageElement = div.querySelector('.message');
  if (text) { // If the message is text.
    messageElement.textContent = text;
    // Replace all line breaks by <br>.
    messageElement.innerHTML = messageElement.innerHTML.replace(/\n/g, '<br>');
  } else if (imageUri) { // If the message is an image.
    var image = document.createElement('img');
    image.addEventListener('load', function() {
      this.messageList.scrollTop = this.messageList.scrollHeight;
    }.bind(this));
    this.setImageUrl(imageUri, image);
    messageElement.innerHTML = '';
    messageElement.appendChild(image);
  }
  // Show the card fading-in and scroll to view the new message.
  setTimeout(function() {div.classList.add('visible')}, 1);
  this.messageList.scrollTop = this.messageList.scrollHeight;
  this.messageInput.focus();
};

// Enables or disables the submit button depending on the values of the input
// fields.
Chat.prototype.toggleButton = function() {
  
  console.log(this.messageInput.value==true);
  console.log(this.messageInput.value);

  if (this.messageInput.value) {
    this.submitButton.removeAttribute('disabled');
  } else {
    this.submitButton.setAttribute('disabled', 'true');
  }
};

// Checks that the Firebase SDK has been correctly setup and configured.
Chat.prototype.checkSetup = function() {
  if (!window.firebase || !(firebase.app instanceof Function) || !firebase.app().options) {
    window.alert('You have not configured and imported the Firebase SDK. ' +
        'Make sure you go through the codelab setup instructions and make ' +
        'sure you are running the codelab using `firebase serve`');
  }
};

//Emoji paste
Chat.prototype.emoji1 = function(){
  this.messageInput.value+= String.fromCodePoint(0x1F600);
}
Chat.prototype.emoji2 = function(){
  this.messageInput.value+= String.fromCodePoint(0x1F605);
}
Chat.prototype.emoji3 = function(){
  this.messageInput.value+= String.fromCodePoint(0x1F619);
}
Chat.prototype.emoji4 = function(){
  this.messageInput.value+= String.fromCodePoint(0x1F914);
}
Chat.prototype.emoji5 = function(){
  this.messageInput.value+= String.fromCodePoint(0x1F917);
}
Chat.prototype.emoji6 = function(){
  this.messageInput.value+= String.fromCodePoint(0x1F62A);
}
Chat.prototype.emoji7 = function(){
  this.messageInput.value+= String.fromCodePoint(0x1F615);
}
Chat.prototype.emoji8 = function(){
  this.messageInput.value+= String.fromCodePoint(0x1F62D);
}
Chat.prototype.emoji9 = function(){
  this.messageInput.value+= String.fromCodePoint(0x1F60D);
}
Chat.prototype.emoji10 = function(){
  this.messageInput.value+= String.fromCodePoint(0x1F924);
}


window.onload = function() {
  window.friendlyChat = new Chat();
};
